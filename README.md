When loading the application the first thing you see is the form to add a user.
 
When the save button is clicked, if the fields have been entered correctly, it saves the user in the database and shows you the user's detail template.
 
In the event that the required fields have not been entered correctly, it shows you an error message (this message can come from html or from those that I have generated manually with Django validators).

