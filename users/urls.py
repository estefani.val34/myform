from django.urls import path

from . import views

urlpatterns = [
    path('', views.users_new, name='users_new'),
    path('users/<int:pk>/', views.users_detail, name='users_detail'),
]