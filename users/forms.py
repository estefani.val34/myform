from django import forms
from django.forms import ModelForm, Textarea
from django.utils.translation import gettext_lazy as _
from .models import User
from django.core.exceptions import ValidationError
import re

class UserForm(forms.ModelForm):
    class Meta:
        model =User
        fields=('__all__')
        exclude = ['created_date']
        widgets = {
           'password': Textarea(attrs={'cols': 90, 'rows': 1}),
           'birth_date': forms.DateTimeInput(attrs={'class': 'datetime-input'})
        }
        labels = {
            'email': _('Email address'),
        }
       
   