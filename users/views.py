from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse
from django.shortcuts import redirect

from .forms import UserForm
from .models import User



def users_detail(request, pk):
    user = get_object_or_404(User,pk=pk)
    return render (request,'users/user_detail.html',{'user':user})

def users_new(request):
    if request.method=="POST":
        form = UserForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.save()
            return redirect('users_detail', pk = user.pk)
    else:
        form= UserForm()
    return render(request,'users/user_edit.html',{'form':form})
